package id.co.unitedapp.unitedapp;

/**
 * Created by esa on 5/11/2017.
 */

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class ItemController extends RecyclerView.ViewHolder implements View.OnClickListener {
    //Variable
    CardView cardItemLayout;
    ImageView icon; // Picture
    TextView title;
    TextView subTitle;

    public ItemController(View itemView) {
        super(itemView);

        //Set id
        cardItemLayout = (CardView) itemView.findViewById(R.id.cardlist_item);

        //Tambahan untuk id Picture
        icon = (ImageView)itemView.findViewById(R.id.icon_item);

        //id Text
        title = (TextView) itemView.findViewById(R.id.listitem_name);
        subTitle = (TextView) itemView.findViewById(R.id.listitem_subname);

        //onClick
        itemView.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        //tampilkan toas ketika click
        //Toast.makeText(v.getContext(), String.format("Position %d", getAdapterPosition()), Toast.LENGTH_SHORT).show();

        if (getAdapterPosition() == 0 ){
            Intent intent = new Intent(v.getContext(), MainActivity.class);
            v.getContext().startActivity(intent);
        }else if (getAdapterPosition() == 1 ){
            Intent intent = new Intent(v.getContext(), MainActivity.class);
            v.getContext().startActivity(intent);
        }else if (getAdapterPosition() == 2 ){
            Intent intent = new Intent(v.getContext(), MainActivity.class);
            v.getContext().startActivity(intent);
        }else if (getAdapterPosition() == 3 ){
            Intent intent = new Intent(v.getContext(), MainActivity.class);
            v.getContext().startActivity(intent);
        }else if (getAdapterPosition() == 4 ){
            Intent intent = new Intent(v.getContext(), MainActivity.class);
            v.getContext().startActivity(intent);
        }else if (getAdapterPosition() == 5 ){
            Intent intent = new Intent(v.getContext(), MainActivity.class);
            v.getContext().startActivity(intent);
        }else if (getAdapterPosition() == 6 ){
            Intent intent = new Intent(v.getContext(), MainActivity.class);
            v.getContext().startActivity(intent);
        }else if (getAdapterPosition() == 7 ){
            Intent intent = new Intent(v.getContext(), MainActivity.class);
            v.getContext().startActivity(intent);
        }
    }
}