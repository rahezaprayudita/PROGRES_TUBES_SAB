package id.co.unitedapp.unitedapp;

/**
 * Created by esa on 5/11/2017.
 */

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Session {
    public static SharedPreferences pref;

    public static Editor editor;
    public static String PREF_NAME = "DataUser";
    public static void createLoginSession(Context context,
                                          String username) {
        pref = context.getSharedPreferences(PREF_NAME, 0);
        editor = pref.edit();
        editor.putString("username", username);
        editor.commit();
    }
    public static void logout(Context context) {
        pref = context.getSharedPreferences(PREF_NAME, 0);
        editor = pref.edit();
        editor.clear();
        editor.commit();
    }
}
