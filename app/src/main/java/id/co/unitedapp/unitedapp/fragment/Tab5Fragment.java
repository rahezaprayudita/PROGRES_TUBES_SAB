package id.co.unitedapp.unitedapp.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import id.co.unitedapp.unitedapp.LoginActivity;
import id.co.unitedapp.unitedapp.R;

/**
 * Created by esa on 5/1/2017.
 */

public class Tab5Fragment extends Fragment {


    public Tab5Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_tab5, container, false);
    }
}
