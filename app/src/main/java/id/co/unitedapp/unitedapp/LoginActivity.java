package id.co.unitedapp.unitedapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by esa on 5/1/2017.
 */

public class LoginActivity extends AppCompatActivity {
    Button btnLogin;
    EditText txtUser, txtPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtUser = (EditText) findViewById(R.id.txt_username);
        txtPass = (EditText) findViewById(R.id.txt_pass);

        btnLogin = (Button) findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = txtUser.getText().toString();
                String password = txtPass.getText().toString();
                if(username.trim().equalsIgnoreCase("")){
                    txtUser.setError("Username Tidak Boleh Kosong");
                    txtUser.requestFocus();
                } else if (password.trim().equalsIgnoreCase("")){
                    txtPass.setError("Password Tidak Boleh Kosong");
                    txtPass.requestFocus();
                } else {
                    if(username.equalsIgnoreCase("raheza") && password.equalsIgnoreCase("raheza")){
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        LoginActivity.this.finish();
                    } else {
                        Toast.makeText(LoginActivity.this, "Username dan Password tidak sesuai", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
}
